package main.java;

import main.java.data_structure.DenseWeightedGraph;
import main.java.data_structure.SparseWeightedGraph;
import main.java.utils.ReadWeightedGraph;

/**
 * @author Wrb
 * @date 2019/6/13 17:13
 */
public class WeightedGraphMain {

	public static void main(String[] args) {
		// 使用两种图的存储方式读取testG1.txt文件
		String filename = "testG1W.txt";
		SparseWeightedGraph<Double> g1 = new SparseWeightedGraph<Double>(8, false);
		ReadWeightedGraph readGraph1 = new ReadWeightedGraph(g1, filename);
		System.out.println("test G1 in Sparse Weighted Graph:");
		g1.show();

		System.out.println();

		DenseWeightedGraph<Double> g2 = new DenseWeightedGraph<Double>(8, false);
		ReadWeightedGraph readGraph2 = new ReadWeightedGraph(g2 , filename );
		System.out.println("test G1 in Dense Graph:");
		g2.show();

		System.out.println();
	}
}
