package main.java.utils;

import main.java.data_structure.*;

import java.util.Random;

/**
 * @author Wrb
 * @date 2019/6/12 16:26
 */
public class UnionFindTestHelper {

	public static void testUF1(int n) {
		UnionFind1 unionFind1 = new UnionFind1(n);
		Random random = new Random();
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind1.unionElements(a, b);
		}
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind1.isConnected(a, b);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("UnionFind1" + " 程序运行时间：" + (endTime - startTime) + "ms");
	}

	public static void testUF2(int n) {
		UnionFind2 unionFind2 = new UnionFind2(n);
		Random random = new Random();
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind2.unionElements(a, b);
		}
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind2.isConnected(a, b);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("UnionFind2" + " 程序运行时间：" + (endTime - startTime) + "ms");
	}

	public static void testUF3(int n) {
		UnionFind3 unionFind3 = new UnionFind3(n);
		Random random = new Random();
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind3.unionElements(a, b);
		}
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind3.isConnected(a, b);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("UnionFind3" + " 程序运行时间：" + (endTime - startTime) + "ms");
	}

	public static void testUF4(int n) {
		UnionFind4 unionFind4 = new UnionFind4(n);
		Random random = new Random();
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind4.unionElements(a, b);
		}
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind4.isConnected(a, b);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("UnionFind3" + " 程序运行时间：" + (endTime - startTime) + "ms");
	}
	public static void testUF5(int n) {
		UnionFind5 unionFind5 = new UnionFind5(n);
		Random random = new Random();
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind5.unionElements(a, b);
		}
		for (int i = 0; i < n; i++) {
			int a = random.nextInt(n);
			int b = random.nextInt(n);
			unionFind5.isConnected(a, b);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("UnionFind3" + " 程序运行时间：" + (endTime - startTime) + "ms");
	}
}
