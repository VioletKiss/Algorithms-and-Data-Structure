package main.java.algorithms;

import main.java.data_structure.MaxHeap;

/**
 * @author Wrb
 * @date 2019/5/28 17:14
 */
public class HeapSort1 implements Sort {
	@Override
	public void sort(int[] arr, int n) {
		MaxHeap maxHeap = new MaxHeap(n);
		for (int i = 0; i < n; i++) {
			maxHeap.insert(arr[i]);
		}
		for (int i = n - 1; i >= 0; i--) {
			arr[i] = maxHeap.extractMax();
		}
	}
}
