package main.java.algorithms;

/**
 * @author Wrb
 * @date 2019/5/22 15:17
 */
public interface Sort {
	void sort(int[] arr, int n);
}
